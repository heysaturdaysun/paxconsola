( @check 1 99 3 99 )

\ canary for success
99

7 case
    6 of 0 print endof
    7 of 1 print endof
    8 of 2 print endof
    ( default ) 3 print
endcase

print
99

9 case
    6 of 0 print endof
    7 of 1 print endof
    8 of 2 print endof
    ( default ) 3 print
endcase

print
